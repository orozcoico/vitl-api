<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\User;

class ApiTest extends TestCase
{
    public function testUserSearchNotFound()
    {
        $this->assertEquals(
            User::search("name that doesn't exist", false)->toArray(), []
        );
    }

    public function testUserSearchSomeUsersFound()
    {
        $this->assertNotEmpty(User::search("Abi", false)->toArray());
    }

    public function testUserSearchSpecificUser()
    {
        $match = [
            [
                "first_name" => "Donna",
                "last_name"  => "Wilson",
            ]
        ];

        $this->assertEquals(
            User::search("Donna Wilson", false)->toArray(), $match
        );
    }


    public function testUserSearchDuplicates()
    {
        $this->assertEquals(User::search("Audrey Wallace", true)->count(), 2);
    }

}

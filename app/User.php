<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    /**
     * Performs a search on the users table and returns matching results
     *
     * @param string $terms search string
     * @param bool $dupes allow duplicated
     * @return mixed
     */
    public static function search(string $terms, bool $dupes = false)
    {
        // Initiate Eloquent
        $users = User::select('first_name', 'last_name');

        // If duplicated are not allowed, then request only distinct elements (based on previous select)
        if (!$dupes) {
            $users = $users->distinct();
        }

        // Complete eloquent with filters (where), apply order and return data
        // Note: I'm using CONCAT_WS to add two columns together with a separator, and perform another where on it
        return $users->where("first_name", "like", "%" . $terms . "%")
            ->orWhere("last_name",  "like", "%" . $terms . "%")
            ->orWhere(DB::raw('CONCAT_WS(" ", first_name, last_name)'),  "like", "%" . $terms . "%")
            ->orderBy('last_name', 'asc')
            ->orderBy('first_name', 'asc')
            ->get();
    }
}

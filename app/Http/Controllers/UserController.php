<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * Retrieve user list matching search request
     *
     * @param Request $request : Request contains terms (search string) and dupes (bool to allow duplicated)
     * @return array
     */
    public function search(Request $request)
    {
        $terms = $request->post("terms");
        $dupes = $request->post("dupes") == "true" ? true : false;

        if (trim($terms) == "") {
            return [];
        }

        return User::search($terms, $dupes);
    }
}
